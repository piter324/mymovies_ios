//
//  SearchViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 20/01/2019.
//  Copyright © 2019 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import CodableFirebase

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var ResultsTableView: UITableView!
    
    let ref = Database.database().reference()
    var userId = Auth.auth().currentUser?.uid ?? ""
    var searchQuery = ""
    
    var friendIds = [String]()
    var friendNicks = [String]()
    var results = [String:[Movie]]()
    
    lazy var spinner:UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        activityView.style = .whiteLarge
        activityView.backgroundColor = UIColor(white: 0.25, alpha: 0.8)
        activityView.center = self.view.center
        activityView.layer.cornerRadius = 10
        return activityView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ResultsTableView.delegate = self
        ResultsTableView.dataSource = self
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.keyboardAppearance = .dark
        searchController.searchBar.delegate = self
        self.view.addSubview(spinner)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let uid = Auth.auth().currentUser?.uid ?? ""
        if uid != userId {
            userId = uid
            performSearch(query: searchQuery)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchQuery = searchBar.text!
        performSearch(query: searchQuery)
        dismiss(animated: true, completion: nil)
    }
    
    
    func performSearch(query:String){
        friendIds.removeAll()
        friendNicks.removeAll()
        results.removeAll()
        ResultsTableView.reloadData()
        if query.count < 1 {
            return
        }
        friendNicks.append("My library")
        friendIds.append(userId)
        spinner.startAnimating()
        ref.child("friends/\(userId)").observeSingleEvent(of: .value, with: {snapshot in
            let friends = snapshot.value as? NSDictionary
            if friends != nil{
                for friend in friends!{
                    let friendId = friend.key as! String
                    let friendVal = friend.value as! [String:String]
                    let friendNick = friendVal["Nick"]
                    
                    self.friendNicks.append(friendNick ?? "")
                    self.friendIds.append(friendId)
                }
            }
            print("Libs: \(self.friendNicks)")
            
            // Get friends' libraries and search through them
            var libsLeft = self.friendIds.count
            for uid in self.friendIds {
                self.ref.child("libraries/\(uid)").observeSingleEvent(of: .value, with: {snapshot in
                    print("Getting library of \(uid)")
                    let libraryItems = snapshot.value as? NSDictionary
                    if libraryItems != nil {
                        self.results[uid] = [Movie]()
                        for item in libraryItems! {
                            do {
                                let model = try FirebaseDecoder().decode(Movie.self, from: item.value)
                                if model.Title.lowercased().range(of: query.lowercased()) != nil { // title matches query
                                    self.results[uid]!.append(model)
                                    print("Adding movie: \(model)")
                                }
                            } catch let err {
                                print("Couldn't decode movie \(err)")
                            }
                        }
                    }
                    libsLeft-=1
                    if libsLeft <= 0{
                        self.ResultsTableView.reloadData()
                        self.spinner.stopAnimating()
                        print(self.results)
                    }
                })
            }
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return friendNicks.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return friendNicks[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results[friendIds[section]]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = ResultsTableView.dequeueReusableCell(withIdentifier: "SearchCell") as? SearchTableViewCell else {
            fatalError("Couldn't dequeue cell")
        }
        
        let movie = results[friendIds[indexPath.section]]![indexPath.row]
        cell.Title.text = movie.Title
        cell.PosterImage.downloadImageFromLink(link: movie.Poster)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = results[friendIds[indexPath.section]]![indexPath.row]
        let movieDetailVC = storyboard?.instantiateViewController(withIdentifier: "OneMovieDetailView") as! MovieDetailsViewController
        movieDetailVC.externalLibrary = true
        movieDetailVC.title = movie.Title
        movieDetailVC.MovieArr.append(movie)
        navigationController?.pushViewController(movieDetailVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
