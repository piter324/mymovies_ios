//
//  FriendsViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 25/10/2018.
//  Copyright © 2018 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import CodableFirebase

class FriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var FriendsList: UITableView!
    
    var friends = [Friend]()
    let ref = Database.database().reference()
    var userId = Auth.auth().currentUser?.uid ?? ""
    
    lazy var refresher:UIRefreshControl = {
        let refreshCtrl = UIRefreshControl()
        refreshCtrl.addTarget(self, action: #selector(getFriendsList), for: .valueChanged)
        return refreshCtrl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FriendsList.delegate = self
        FriendsList.dataSource = self
        FriendsList.refreshControl = refresher
        getFriendsList()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getFriendsList), name: NSNotification.Name("FriendsChanged"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let uid = Auth.auth().currentUser?.uid ?? ""
        if userId != uid {
            self.userId = uid
            getFriendsList()
        }
    }
    
    @objc
    func getFriendsList(){
        self.friends.removeAll()
        self.FriendsList.reloadData()
        ref.child("friends/\(userId)").observeSingleEvent(of: .value, with: {snapshot in
            let fds = snapshot.value as? NSDictionary
            print("Found friends: \(fds?.count)")
            if fds != nil{
                self.friends.removeAll()
                for friend in fds!{
                    do {
                        let model = try FirebaseDecoder().decode(Friend.self, from: friend.value)
                        self.friends.append(model)
                    } catch let error {
                        print("Error in decoding friends: \(error)")
                    }
                }
                self.FriendsList.reloadData()
                self.refresher.endRefreshing()
            }
        })
    }
    
    func removeFriend(uid:String){
        print("Removing friend: \(uid)")
        ref.child("friends/\(userId)/\(uid)").removeValue()
        ref.child("friends/\(uid)/\(userId)").removeValue()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = FriendsList.dequeueReusableCell(withIdentifier: "FriendRow") as? FriendTableViewCell else {
            fatalError("Couldn't attach to FriendTableViewCell")
        }
        let i = indexPath.row
        cell.Nick.text = friends[i].Nick
        cell.Avatar.downloadAvatarFromFirebase(filenameNoExt: friends[i].Uid)
        cell.Avatar.asCircle()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let libraryVC = storyboard?.instantiateViewController(withIdentifier: "LibraryVC") as? ViewController
        libraryVC?.externalLibrary = true
        let friend = friends[indexPath.row]
        libraryVC?.title = friend.Nick+"'s Library"
        libraryVC?.userId = friend.Uid
        navigationController?.pushViewController(libraryVC!, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let i = indexPath.row
            removeFriend(uid: friends[i].Uid)
            friends.remove(at: i)
            FriendsList.deleteRows(at: [indexPath], with: .fade)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
