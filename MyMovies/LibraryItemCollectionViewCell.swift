//
//  LibraryItemCollectionViewCell.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 26/10/2018.
//  Copyright © 2018 Piotr Muzyczuk. All rights reserved.
//

import UIKit

class LibraryItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var MoviePoster: UIImageView!
}
