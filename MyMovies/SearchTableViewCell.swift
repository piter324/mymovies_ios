//
//  SearchTableViewCell.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 20/01/2019.
//  Copyright © 2019 Piotr Muzyczuk. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var PosterImage: UIImageView!
    @IBOutlet weak var Title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
