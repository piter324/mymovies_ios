//
//  ReceivedRequestsViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 07/01/2019.
//  Copyright © 2019 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import CodableFirebase

class ReceivedRequestsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var RRtableView: UITableView!
    
    let userId = Auth.auth().currentUser!.uid
    override func viewDidLoad() {
        super.viewDidLoad()

        RRtableView.delegate = self
        RRtableView.dataSource = self
        // Do any additional setup after loading the view.
        reloadRequests()
    }
    var requests = [Friend]()
    let ref = Database.database().reference()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FriendsChanged"), object: nil)
    }
    
    func reloadRequests(){
        self.requests.removeAll()
        self.RRtableView.reloadData()
        ref.child("friendRequestsReceived/\(userId)").observeSingleEvent(of: .value, with: {snapshot in
            let wannabeFriends = snapshot.value as? NSDictionary
            print("Found requests: \(wannabeFriends?.count)")
            if wannabeFriends != nil{
                self.requests.removeAll()
                for friend in wannabeFriends! {
                    do{
                        let model = try FirebaseDecoder().decode(Friend.self, from: friend.value)
                        self.requests.append(model)
                    } catch let error{
                        print("Error decoding requests: \(error)")
                    }
                }
                self.RRtableView.reloadData()
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requests.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = RRtableView.dequeueReusableCell(withIdentifier: "ReceivedRequestCell") as? ReceivedRequestCell else {
            fatalError("Couldn't dequeue received requests cells")
        }
        let i = indexPath.row
        cell.Avatar.downloadAvatarFromFirebase(filenameNoExt: requests[i].Uid)
        cell.Avatar.asCircle()
        cell.Nick.text = requests[i].Nick
        cell.AcceptBtn.tag = i
        cell.AcceptBtn.addTarget(self, action: #selector(acceptRequest), for: .touchUpInside)
        cell.DeleteBtn.tag = i
        cell.DeleteBtn.addTarget(self, action: #selector(deleteRequest), for: .touchUpInside)
        return cell
    }
    
    @objc func acceptRequest(sender:UIButton){
        print("Request accepted")
        let ind = sender.tag
        let friendUid = requests[ind].Uid
        do {
            let newFriendData = try FirebaseEncoder().encode(requests[ind]) as! [String:Any]
            ref.child("friends/\(userId)/\(friendUid)").setValue(newFriendData)
            ref.child("users/\(userId)").observeSingleEvent(of: .value, with: {snapshot in
                let myInfo = snapshot.value as! NSDictionary
                self.ref.child("friends/\(friendUid)/\(self.userId)").setValue(myInfo)
            })
            
            ref.child("friendRequestsReceived/\(userId)/\(friendUid)").removeValue()
            ref.child("friendRequestsReceived/\(friendUid)/\(userId)").removeValue()
            reloadRequests()
        } catch let err {
            print("Error accepting friend: \(err)")
        }
    }
    
    @objc func deleteRequest(sender:UIButton){
        print("Request rejected")
        let ind = sender.tag
        ref.child("friendRequestsReceived/\(userId)/\(requests[ind].Uid)").removeValue()
        ref.child("friendRequestsReceived/\(requests[ind].Uid)/\(userId)").removeValue()
        reloadRequests()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
