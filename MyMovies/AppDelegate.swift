//
//  AppDelegate.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 25/10/2018.
//  Copyright © 2018 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

struct Movie:Codable {
    let Title:String
    let imdbRating:String
    let imdbID:String
    let Year:String
    let Runtime:String
    let Plot:String
    let Genre:String
    let Poster:String
    let Notes:String
    let Favorite:Bool
    let AddedDate:Double
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.Title = try container.decDef(key: .Title, defaultValue: "(no title)")
        self.imdbRating = try container.decDef(key: .imdbRating, defaultValue: "(no rating)")
        self.imdbID = try container.decodeIfPresent(String.self, forKey: .imdbID) ?? ""
        self.Year = try container.decDef(key: .Year, defaultValue: "(no year)")
        self.Runtime = try container.decDef(key: .Runtime, defaultValue: "(no runtime)")
        self.Plot = try container.decDef(key: .Plot, defaultValue: "(no plot decription)")
        self.Genre = try container.decDef(key: .Genre, defaultValue: "(no genre)")
        self.Poster = try container.decDef(key: .Poster, defaultValue: "(no poster)")
        self.Notes = try container.decDef(key: .Notes, defaultValue: "")
        self.Favorite = try container.decDef(key: .Favorite, defaultValue: false)
        self.AddedDate = try container.decDef(key: .AddedDate, defaultValue: 0)
    }
}

struct Friend:Codable{
    let Uid:String
    let Nick:String
}

extension KeyedDecodingContainer {
    func decDef<T>(key: K, defaultValue: T) throws -> T where T : Decodable{
        return try decodeIfPresent(T.self, forKey: key) ?? defaultValue
    }
}

extension UIImageView {
    func asCircle(){
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
    }
    
    func downloadAvatarFromFirebase(filenameNoExt:String){
        let noAvatarLink = "https://firebasestorage.googleapis.com/v0/b/mymovies-91b8f.appspot.com/o/avatars%2Fdefault_user.png?alt=media&token=7d93f21b-6c7e-4a0e-acab-8b36954160e1"
        let avatarRef = Storage.storage().reference().child("avatars/\(filenameNoExt).jpg")
        avatarRef.downloadURL(completion: {(url, error) in
            if let error = error {
                print("Error getting download link for avatar: \(error)")
                self.downloadImageFromLink(link: noAvatarLink)
                return
            } else {
                if url != nil{
                    self.downloadImageFromLink(link: url!.absoluteString)
                } else {
                    self.downloadImageFromLink(link: noAvatarLink)
                }
            }
        })
    }
    
    func downloadImageFromLink(link: String){
        guard let linkURL = URL(string: link) else {
            self.image = UIImage(named: "no_photo.png")
            return
        }
        URLSession.shared.dataTask(with: linkURL) { (data,response,error) in
            
            DispatchQueue.main.async {
                if let data = data {
                    guard let image = UIImage(data: data) else { return }
                    self.image = image
                    print("Image downloaded")
                } else {
                    self.image = UIImage(named: "no_photo.png")
                }
            }
        }.resume()
    }
}

extension UIActivityIndicatorView{
    func stopSpinningMainThread(){
        DispatchQueue.main.async {
            self.stopAnimating()
        }
    }
}

extension UIViewController{
    func showErrorAlert(message: String, mainThread:Bool){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert )
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(cancelAction)
        if(mainThread) {
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }
        else {
            self.present(alert, animated: true, completion: nil)
        }
    }
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    override init(){
        FirebaseApp.configure()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
