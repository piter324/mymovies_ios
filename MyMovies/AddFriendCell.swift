//
//  AddFriendCell.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 07/01/2019.
//  Copyright © 2019 Piotr Muzyczuk. All rights reserved.
//

import UIKit

class AddFriendCell: UITableViewCell {

    @IBOutlet weak var AvatarImg: UIImageView!
    @IBOutlet weak var Nick: UILabel!
    @IBOutlet weak var AddBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
