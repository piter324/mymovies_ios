//
//  FriendTableViewCell.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 27/10/2018.
//  Copyright © 2018 Piotr Muzyczuk. All rights reserved.
//

import UIKit

class FriendTableViewCell: UITableViewCell {

    @IBOutlet weak var Avatar: UIImageView!
    @IBOutlet weak var Nick: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
