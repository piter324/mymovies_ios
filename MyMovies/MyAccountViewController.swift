//
//  MyAccountViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 25/10/2018.
//  Copyright © 2018 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import CodableFirebase
import FirebaseStorage

class MyAccountViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var AvatarImg: UIImageView!
    @IBOutlet weak var EmailLbl: UILabel!
    
    let ref = Database.database().reference()
    let imagePicker = UIImagePickerController()
    var userId = Auth.auth().currentUser?.uid ?? ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AvatarImg.asCircle()
        imagePicker.delegate = self
        getUserInfo()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let uid = Auth.auth().currentUser?.uid ?? ""
        if userId != uid {
            self.userId = uid
            getUserInfo()
        }
    }
    
    @IBAction func LogOutBtn_Click(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginViewController
            self.present(loginVC!, animated: true, completion: nil)
        } catch let signOutError as NSError {
            self.showErrorAlert(message: "Error signing out \(signOutError)", mainThread: true)
        }
    }
    @IBAction func ChangeAvatarBtn_Click(_ sender: Any) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            var data = Data()
            data = pickedImage.jpegData(compressionQuality: 0.5)!
            let meta = StorageMetadata()
            meta.contentType = "image/jpeg"
            let storageRefChild = Storage.storage().reference().child("avatars/\(userId).jpg")
            storageRefChild.putData(data, metadata: meta){ (metadata,error) in
                guard let metadata = metadata else {
                    self.showErrorAlert(message: "Couldn't upload your avatar", mainThread: true)
                    print("Couldn't upload")
                    return
                }
                self.getUserInfo()
            }
        }
    }
    
    func getUserInfo(){
        print(userId)
        self.AvatarImg.downloadImageFromLink(link: "")
        self.EmailLbl.text = ""
        ref.child("users/\(userId)").observeSingleEvent(of: .value, with: {snapshot in
            let myInfo = snapshot.value as? NSDictionary
            print(myInfo)
            if myInfo != nil{
                do{
                    let modal = try FirebaseDecoder().decode(Friend.self, from: myInfo!)
                    self.AvatarImg.downloadAvatarFromFirebase(filenameNoExt: modal.Uid)
                    self.EmailLbl.text = modal.Nick
                } catch let err {
                    print("Error obtaining user information: \(err)")
                }
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
