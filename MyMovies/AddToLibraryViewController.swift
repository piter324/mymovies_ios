//
//  AddToLibraryViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 25/10/2018.
//  Copyright © 2018 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseDatabase
import CodableFirebase
import FirebaseAuth

class AddToLibraryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    struct movieFromOMDb:Codable {
        let Title:String
        let imdbRating:String
        let imdbID:String
        let Year:String
        let Runtime:String
        let Plot:String
        let Genre:String
        let Poster:String
    }
    struct searchedMovie:Decodable {
        let Title:String
        let imdbID:String
        let Year:String
        let Poster:String
    }
    struct searched:Decodable {
        let Search:[searchedMovie]
    }
    
    lazy var spinner:UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        activityView.style = .whiteLarge
        activityView.backgroundColor = UIColor(white: 0.25, alpha: 0.8)
        activityView.center = self.view.center
        activityView.layer.cornerRadius = 10
        return activityView
    }()
    
    
    @IBOutlet weak var moviesTableView: UITableView!
    
    var moviesFound = [searchedMovie]()
    var libraryItems = [String]()
    let dbRef = Database.database().reference()
    var userId = ""
    var handle:AuthStateDidChangeListenerHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        moviesTableView.delegate = self
        moviesTableView.dataSource = self
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.keyboardAppearance = .dark
        searchController.searchBar.delegate = self
        self.view.addSubview(spinner)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        self.getLibraryItems()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.handle = Auth.auth().addStateDidChangeListener{ (auth, user) in
            if user != nil {
                self.userId = user!.uid
                self.getLibraryItems()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(self.handle!)
    }
    
    func getLibraryItems(){
        dbRef.child("libraries/\(userId)").observeSingleEvent(of: .value, with: {snapshot in
            let library = snapshot.value as? NSDictionary
            if library != nil {
                for item in library!{
                    let itemId = item.key as! String
                    self.libraryItems.append(itemId)
                }
            }
        })
    }
    
    func searchOMDb(query:String){
        let encodedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        guard let linkURL = URL(string: "https://www.omdbapi.com/?apikey=9a209949&type=movie&s=\(encodedQuery)") else { return }
        spinner.startAnimating()
        URLSession.shared.dataTask(with: linkURL) { (data,response,error) in
                if let data = data {
                    do{
                        let allResults = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                        if let APIerr = allResults["Error"] {
                            self.showErrorAlert(message: APIerr as! String, mainThread: true)
                        }
                        else{
                            let results = try JSONDecoder().decode(searched.self, from: data)
                            self.moviesFound = results.Search
                            print("Search results downloaded for: \(query)")
                            DispatchQueue.main.async {
                                self.moviesTableView.reloadData()
                            }
                        }
                        
                    } catch let searchErr {
                        print("Error getting search results: ", searchErr)
                        self.showErrorAlert(message: "There's been an error with parsing search results", mainThread: true)
                    }
                }
                self.spinner.stopSpinningMainThread()

            }.resume()
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveMovieToFirebaseLibrary(imdbID:String){
        guard let linkURL = URL(string: "https://www.omdbapi.com/?apikey=9a209949&type=movie&i=\(imdbID)") else { return }
        spinner.startAnimating()
        URLSession.shared.dataTask(with: linkURL) { (data,response,error) in
            if let data = data {
                do{
                    let result = try JSONDecoder().decode(movieFromOMDb.self, from: data)
                    
                    //save that to Firebase
                    var FBdata = try FirebaseEncoder().encode(result) as! [String:Any]
                    FBdata["Notes"] = ""
                    FBdata["AddedDate"] = Date().timeIntervalSinceReferenceDate
                    FBdata["Favorite"] = false
                    self.dbRef.child("libraries/\(self.userId)/\(result.imdbID)").setValue(FBdata)
                    print("Data added to Firebase")
                    self.libraryItems.append(result.imdbID)
//                    DispatchQueue.main.async {
//                        self.dismiss(animated: true, completion: nil)
//                    }
                } catch let FBerr {
                    print("Error saving to Firebase: ", FBerr)
                    self.showErrorAlert(message: "Error saving your movie to Firebase", mainThread: true)
                }
            }
            self.spinner.stopSpinningMainThread()
        }.resume()
    }
    
    @IBAction func cancelSearchModal(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LibraryChanged"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let query:String = searchBar.text!
        if query.count >= 3{
            searchOMDb(query: query)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { //how many cells are in our array
        return moviesFound.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { //populate that cell in an automatic loop
        guard let cell = moviesTableView.dequeueReusableCell(withIdentifier: "MovieCell") as? MovieSearchTableViewCell else {
            fatalError("Couldn't attach to MovieSearchTableViewCell")
        }
        cell.MovieTitle.text = moviesFound[indexPath.row].Title
        cell.MovieReleased.text = "Released: "+moviesFound[indexPath.row].Year
        cell.MoviePoster.downloadImageFromLink(link: moviesFound[indexPath.row].Poster)
        cell.AddButton.tag = indexPath.row
        if libraryItems.contains(moviesFound[indexPath.row].imdbID) {
            cell.AddButton.isEnabled = false
        } else {
            cell.AddButton.isEnabled = true
        }
        cell.AddButton.addTarget(self, action: #selector(addBtnTouchUp), for: .touchUpInside)
        return cell
    }
    
   // func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   //     saveMovieToFirebaseLibrary(imdbID: moviesFound[indexPath.row].imdbID)
   // }
    
    @objc func addBtnTouchUp(sender:UIButton){
        //print("Sender: \(sender.tag)")
        saveMovieToFirebaseLibrary(imdbID: moviesFound[sender.tag].imdbID)
        sender.isEnabled = false
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
