//
//  ViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 25/10/2018.
//  Copyright © 2018 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseDatabase
import CodableFirebase
import FirebaseAuth

class ViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UIViewControllerPreviewingDelegate {
    
    var externalLibrary:Bool = false
    
    var handle:AuthStateDidChangeListenerHandle?
    var userId:String = ""
    var library = [Movie]()
    let dbRef = Database.database().reference()
    lazy var refresher:UIRefreshControl = {
        let refreshCtrl = UIRefreshControl()
        refreshCtrl.addTarget(self, action: #selector(loadLibrary), for: .valueChanged)
        return refreshCtrl
    }()
    
    @IBOutlet weak var moviesGrid: UICollectionView!
    
    @IBOutlet weak var AddToLibraryBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moviesGrid.delegate = self
        moviesGrid.dataSource = self
        moviesGrid.refreshControl = refresher
        
        // Do any additional setup after loading the view, typically from a nib.
        if Auth.auth().currentUser == nil{
            performSegue(withIdentifier: "LoginVCmodal", sender: self)
            print("Should display auth dialog")
        } else {
            if !externalLibrary {
                let user = Auth.auth().currentUser
                self.userId = user!.uid
            }
            loadLibrary()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadLibrary), name:NSNotification.Name(rawValue: "LibraryChanged"), object: nil)
        
        // Checking for 3D touch support
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: view)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !externalLibrary {
            handle = Auth.auth().addStateDidChangeListener { (auth, user) in
                if user != nil {
                    if user!.uid != self.userId {
                        self.userId = user!.uid
                        self.loadLibrary()
                    }
                }
            }
            AddToLibraryBtn.isEnabled = true
        } else {
            AddToLibraryBtn.isEnabled = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    var movieIndexToPop = -1
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        return instantiateVCforMovieDetail(movie: library[movieIndexToPop])
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        let detailVC = instantiateVCforMovieDetail(movie: library[movieIndexToPop])
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func instantiateVCforMovieDetail(movie:Movie) -> UIViewController {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OneMovieDetailView") as! MovieDetailsViewController
        let cMovie = movie
        vc.title = cMovie.Title
        vc.externalLibrary = self.externalLibrary
        vc.userId = self.userId
        vc.MovieArr.append(cMovie)
        return vc
    }
    
    @objc
    func loadLibrary(){
//        if Auth.auth().currentUser == nil { return } else { self.userId = Auth.auth().currentUser!.uid}
        print(self.userId)
        
        self.library.removeAll(keepingCapacity: false)
        self.moviesGrid.reloadData()
        dbRef.child("libraries/\(self.userId)").observeSingleEvent(of: .value, with: {snapshot in
            let downloadedLibrary = snapshot.value as? NSDictionary
            if downloadedLibrary != nil {
                do {
                    self.library.removeAll(keepingCapacity: true)
                    for libraryItem in downloadedLibrary!{
                        let model = try FirebaseDecoder().decode(Movie.self, from: libraryItem.value)
                        self.library.append(model)
                    }
                    self.library = self.library.sorted(by: { $0.Title < $1.Title })
                    self.moviesGrid.reloadData()
                } catch let error {
                    self.loadLibraryError()
                    print(error)
                }
                print("Library loaded")
            }
            self.refresher.endRefreshing()
        })
    }
    
    func loadLibraryError(){
        let alert = UIAlertController(title: "Error", message: "There's been an error loading your library", preferredStyle: .alert )
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding:CGFloat = 10
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/1.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return library.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = moviesGrid.dequeueReusableCell(withReuseIdentifier: "LibraryItem", for: indexPath) as? LibraryItemCollectionViewCell else {
            fatalError("Couldn't downcast UICollectionViewCell to LibraryItem")
        }
        
        cell.MoviePoster.downloadImageFromLink(link: library[indexPath.row].Poster)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = instantiateVCforMovieDetail(movie: library[indexPath.row])
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        print("Highlighted: \(indexPath.row)")
        self.movieIndexToPop = indexPath.row
    }

}

