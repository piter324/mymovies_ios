//
//  AddFriendTableViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 02/01/2019.
//  Copyright © 2019 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import CodableFirebase

class AddFriendTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    lazy var spinner:UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        activityView.style = .whiteLarge
        activityView.backgroundColor = UIColor(white: 0.25, alpha: 0.8)
        activityView.center = self.view.center
        activityView.layer.cornerRadius = 10
        return activityView
    }()
    
    var usersFound = [Friend]()
    var friendsFound = [String]()
    let ref = Database.database().reference()
    let userId:String = Auth.auth().currentUser?.uid ?? ""
    
    @IBOutlet var AddFriendsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AddFriendsTableView.delegate = self
        AddFriendsTableView.dataSource = self
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.keyboardAppearance = .dark
        searchController.searchBar.delegate = self
        self.view.addSubview(spinner)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func searchNicks(nick:String){
        spinner.startAnimating();
        ref.child("friends/\(userId)").observeSingleEvent(of: .value, with: {snapshot in
            let allFriends = snapshot.value as? NSDictionary
            print("Got current friends: \(allFriends?.count)")
            self.friendsFound.removeAll()
            if allFriends != nil {
                for friend in allFriends! {
                    let friendVal = friend.value as? [String:String]
                    if friendVal != nil && friendVal!["Uid"] != nil {
                        self.friendsFound.append(friendVal!["Uid"]!)
                    }
                }
                print(self.friendsFound)
            }
            self.ref.child("users").observeSingleEvent(of: .value, with: {snapshot2 in
                let allUsers = snapshot2.value as? NSDictionary
                print("Got all users: \(allUsers?.count)")
                self.usersFound.removeAll()
                if allUsers != nil {
                    for oneUser in allUsers!{
                        do{
                            let model = try FirebaseDecoder().decode(Friend.self, from: oneUser.value)
                            print("Nick: "+model.Nick)
                            if model.Nick.lowercased().range(of: nick.lowercased()) != nil { // nick matches
                                if model.Uid != self.userId && !self.friendsFound.contains(model.Uid) { // not myself
                                    self.usersFound.append(model)
                                }
                            }
                        } catch let error{
                            print("Decoding failed \(error)")
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.spinner.stopAnimating()
                    self.AddFriendsTableView.reloadData()
                }
            })
        })
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let query:String = searchBar.text!
        searchNicks(nick: query)
        dismiss(animated: true, completion: nil)
        print("Friend search initiated")
    }
    
    func saveNewFriend(friendId:String){
        ref.child("users/\(userId)").observeSingleEvent(of: .value, with: {snapshot in
            let myUserData = snapshot.value as? NSDictionary
            if myUserData != nil {
                self.ref.child("friendRequestsReceived/\(friendId)/\(self.userId)").setValue(myUserData)
                print("Friend saved")
                self.dismiss(animated: true, completion: nil)
            }
        })
    }

    @IBAction func CloseBtn_Click(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("to display: \(usersFound.count)")
        return usersFound.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = AddFriendsTableView.dequeueReusableCell(withIdentifier: "AddFriendCell") as? AddFriendCell else {
            fatalError("Couldn't dequeue add friend cells")
        }
        let i = indexPath.row
        cell.AvatarImg.downloadAvatarFromFirebase(filenameNoExt: usersFound[i].Uid)
        cell.AvatarImg.asCircle()
        cell.Nick.text = usersFound[i].Nick
        cell.AddBtn.tag = indexPath.row
        cell.AddBtn.addTarget(self, action: #selector(addFriendBtnTouchUp), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125.0
    }
    
    @objc func addFriendBtnTouchUp(sender:UIButton){
        print(usersFound[sender.tag].Uid)
        saveNewFriend(friendId: usersFound[sender.tag].Uid)
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
