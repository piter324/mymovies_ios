//
//  MovieDetailsViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 26/10/2018.
//  Copyright © 2018 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseDatabase

class MovieDetailsViewController: UIViewController {

    let dbRef = Database.database().reference()
    var userId = ""
    var externalLibrary = false
    
    @IBOutlet weak var RemoveFromLibraryBtn: UIButton!
    @IBOutlet weak var Plot: UILabel!
    @IBOutlet weak var Runtime: UILabel!
    @IBOutlet weak var Year: UILabel!
    @IBOutlet weak var Rating: UILabel!
    @IBOutlet weak var Poster: UIImageView!
    var MovieArr = [Movie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RemoveFromLibraryBtn.layer.cornerRadius = 5
        if externalLibrary {
           RemoveFromLibraryBtn.isHidden = true
        }
        
        if MovieArr.count > 0 {
            let cMovie = MovieArr[0]
            Poster.downloadImageFromLink(link: cMovie.Poster)
            Rating.text = cMovie.imdbRating
            Runtime.text = cMovie.Runtime
            Year.text = cMovie.Year
            Plot.text = cMovie.Plot
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func RemoveFromLibraryBtn_Click(_ sender: Any) {
        if MovieArr.count > 0 {
            let cMovie = MovieArr[0]
            dbRef.child("libraries/\(userId)/\(cMovie.imdbID)").removeValue { (error:Error?, ref:DatabaseReference) in
                if let error = error {
                    self.showErrorAlert(message: "Movie could't be deleted: \(error)", mainThread: true)
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LibraryChanged"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
