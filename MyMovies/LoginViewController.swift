//
//  LoginViewController.swift
//  MyMovies
//
//  Created by Piotr Muzyczuk on 02/01/2019.
//  Copyright © 2019 Piotr Muzyczuk. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: UIViewController {

    @IBOutlet weak var actionSwitcher: UISegmentedControl!
    
    @IBOutlet weak var SignInView: UIView!
    @IBOutlet weak var EmailTxtField: UITextField!
    @IBOutlet weak var PasswordTxtField: UITextField!
    
    @IBOutlet weak var SignUpView: UIView!
    @IBOutlet weak var SUemail: UITextField!
    @IBOutlet weak var SUpass: UITextField!
    @IBOutlet weak var SUnickname: UITextField!
    
    let ref = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Login VC loaded")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionSwitcherValueChanged(_ sender: Any) {
        if actionSwitcher.selectedSegmentIndex == 0 {
            SignInView.isHidden = false
            SignUpView.isHidden = true
        } else {
            SignUpView.isHidden = false
            SignInView.isHidden = true
        }
    }
    
    
    @IBAction func SignUpBtn_Click(_ sender: Any) {
        Auth.auth().createUser(withEmail: SUemail.text!, password: SUpass.text!) { (authResult, error) in
            if let error = error {
                self.showErrorAlert(message: "Error: \(error.localizedDescription)", mainThread: true)
                return
            }
            let userInfo = [
                "Uid":authResult?.user.uid,
                "Nick":self.SUnickname.text!,
                "About":""
            ]
            let uid:String = authResult!.user.uid
            self.ref.child("users/\(uid)").setValue(userInfo)
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func SignInBtn_Click(_ sender: Any) {
        Auth.auth().signIn(withEmail: EmailTxtField.text!, password: PasswordTxtField.text!) { (authResult, error) in
            if let error = error {
                self.showErrorAlert(message: "Error: \(error.localizedDescription)", mainThread: true)
            } else {
//                print(authResult?.user.uid)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
