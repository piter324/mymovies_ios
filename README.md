# MyMovies
MyMovies is a small app that helps you keep your movie collection organized. It also allows to share it with friends so everyone knows what interesting films others have.

The slideshow presentation of the app is available in a PDF format in this repo.